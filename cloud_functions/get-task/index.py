import os
import ydb

def get_task(key):
    driver_config = ydb.DriverConfig(
        os.environ["YDB_ENDPOINT"], os.environ["YDB_DATABASE"],
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        return driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            f'SELECT * FROM tasks WHERE id="{key}";',
            commit_tx=True,
        )[0].rows[0]


def handler(event, context):
    key = event['queryStringParameters']['key']

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': get_task(key)
    }

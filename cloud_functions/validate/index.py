import os
import ydb
import boto3

def set_status_to_processing(key):
    driver_config = ydb.DriverConfig(
        os.environ["YDB_ENDPOINT"], os.environ["YDB_DATABASE"],
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            f'UPDATE tasks SET status = "PROCESSING" WHERE id = "{key}";',
            commit_tx=True,
        )

def put_message_to_the_queue(key):
    client = boto3.client(
        service_name='sqs',
        endpoint_url='https://message-queue.api.cloud.yandex.net',
        region_name='ru-central1'
    )

    queue_url = client.create_queue(QueueName='ymq_example_boto3').get('QueueUrl')
    print('Created queue url is "{}"'.format(queue_url))

    client.send_message(
        QueueUrl="https://message-queue.api.cloud.yandex.net/b1g41ugvrcobabibvalm/dj6000000001nr2006nf/transfer-tasks",
        MessageBody=key
    )

def handler(event, context):
    key = event['messages'][0]['details']['object_id']

    set_status_to_processing(key)

    put_message_to_the_queue(key)

import boto3
import os
import styletransfer as st
from kikimr.public.sdk.python import client as ydb

def set_status_done(key):
    driver_config = ydb.DriverConfig(
        os.environ["YDB_ENDPOINT"], os.environ["YDB_DATABASE"],
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            f'UPDATE tasks SET status = "DONE" WHERE id = "{key}";',
            commit_tx=True,
        )

def get_model_name(key):
    driver_config = ydb.DriverConfig(
        os.environ["YDB_ENDPOINT"], os.environ["YDB_DATABASE"],
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        return driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            f'SELECT * FROM tasks WHERE id="{key}";',
            commit_tx=True,
        )[0].rows[0]["model"].decode("utf-8") 


def put_message_to_the_queue(key):
    client = boto3.client(
        service_name='sqs',
        endpoint_url='https://message-queue.api.cloud.yandex.net',
        region_name='ru-central1'
    )

    queue_url = client.create_queue(QueueName='ymq_example_boto3').get('QueueUrl')
    print('Created queue url is "{}"'.format(queue_url))

    client.send_message(
        QueueUrl="https://message-queue.api.cloud.yandex.net/b1g41ugvrcobabibvalm/dj6000000001nr2006nf/transfer-tasks",
        MessageBody=key
    )

def handler(event, context):
    upload_bucket = "upload-aliia"
    result_bucket= "result-aliia"

    session = boto3.session.Session(region_name='ru-central1',
                                aws_access_key_id=os.environ["AWS_ACCESS_KEY_ID"],
                                aws_secret_access_key=os.environ["AWS_SECRET_ACCESS_KEY"])
        
    s3 = session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net'
    )

    for msg in event["messages"]:
        key = msg["details"]["message"]["body"]
        
        get_object_response = s3.get_object(Bucket=upload_bucket,Key=key)
        image = get_object_response['Body'].read()

        # ext = '.' + key.split('.')[-1]

        ext = '.jpg'

        image_out = st.process_image(image, ext, get_model_name(key))

        s3.put_object(Bucket=result_bucket, Key=key, Body=image_out)

        set_status_done(key)

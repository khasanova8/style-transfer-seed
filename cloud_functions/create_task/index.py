import uuid
import boto3
import os
import ydb

def put_new_task_to_db(key, model):
    driver_config = ydb.DriverConfig(
        os.environ["YDB_ENDPOINT"], os.environ["YDB_DATABASE"],
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            f'INSERT INTO tasks (id, status, result_url, model) VALUES ("{key}", "NEW", "https://storage.yandexcloud.net/result-aliia/{key}", "{model}");',
            commit_tx=True,
        )


def handler(event, context):
    model = event['queryStringParameters']['model']
    session = boto3.session.Session(region_name='ru-central1',
                                aws_access_key_id=os.environ["AWS_ACCESS_KEY_ID"],
                                aws_secret_access_key=os.environ["AWS_SECRET_ACCESS_KEY"])
        
    s3 = session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net'
    )
    
    bucket = "upload-aliia"
    key = str(uuid.uuid4())
    put_new_task_to_db(key, model)
    presigned_url = s3.generate_presigned_post(bucket, key)

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': presigned_url
    }
